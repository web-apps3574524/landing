@extends ('layouts.default')
    @section ('footer')
        <footer>
            <div class="landpage-footer-background">
            <!-- config in ./config/footer-menu.php -->
                <div class="landpage-footer-menu-container">
                    {!! $footerBarMenuMain->asUl() !!}
                </div>
                <div class="footer-love-final">
                    <div class="footer-love-text">
                        <p>Made with  and with </p>
                    </div>
                    <div class="footer-laravel-and-php-versions">
                        <p> Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})</p>
                    </div>
                </div>
            </div>
        </footer>
    @endsection
