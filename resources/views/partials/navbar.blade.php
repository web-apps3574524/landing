@extends ('layouts.default')
@section ('navbar')
    <header>
        <div class="landpage-navbar">
            <div class="landpage-navbar-logo">
                <img src={{asset('images/logo.svg')}} alt="logo"></img>
            </div>
            <div class="landpage-navbar-title">
                <div class="landpage-navbar-title-text">
                    <div>{{ config('app.name') }}</div>
                </div>
            </div>
            <div class="landpage-navbar-authblock-container">
                <div class="authblock">
                    @if (Route::has('login'))
                        @auth
                            <a href="{{ url('/home') }}" class="authblock-home-part"> Home</a>
                        @else
                            <a href="{{ route('login') }}" class="authblock-login-part">﫻 Log in</a>
                            @if (Route::has('register'))
                                <a href="{{ route('register') }}" class="authblock-register-part">ﰳ Register</a>
                            @endif
                        @endauth
                    @endif
                </div>
            </div>
        </div>
        <nav>
        <!-- configure content in ./config/navbar-menu.php -->
            <div class="landpage-navbar-menu">
                {!! $navBarMenuMain->asUl() !!}
            </div>
        </nav>
    </header>
@endsection
