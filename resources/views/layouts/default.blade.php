<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/colors.css')}}">
    <link rel="stylesheet" href="{{asset('css/fonts.css')}}">
    <link rel="stylesheet" href="{{asset('css/keyframes.css')}}">
    <link rel="stylesheet" href="{{asset('css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('css/shortText.css')}}">
    <link rel="stylesheet" href="{{asset('css/assets.css')}}">
    <link rel="stylesheet" href="{{asset('css/footer.css')}}">
    <title>{{ config('app.name') }}</title>
    <!-- Matomo -->
    <script>
      var _paq = window._paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="//analytics.framend.com/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '1']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
    <noscript>
    <!-- Matomo Image Tracker-->
    <img referrerpolicy="no-referrer-when-downgrade" src="https://analytics.framend.com/matomo.php?idsite=1&amp;rec=1" style="border:0" alt="" />
    </noscript>
</head>
<body>
    @yield('navbar')
    @yield('landpageText')
    @yield('landpageAssets')
    @yield('footer')
</body>
</html>
