@extends('layouts.default')
@section('navbar')
    @include('partials.navbar')
@endsection
@section('landpageText')
    @include ('partials.landpageText')
@endsection
@section('landpageAssets')
    @include ('partials.landpageAssets')
@endsection
@section('footer')
    @include('partials.footer')
@endsection
