# Landing
This project aims to be a minimal front-end website based on Laravel.

Elements of the website are stored in config files located in ./config, frugal way.

Middlewares that controls all that are stored in ./app/Http/Middleware/GenerateMenus.php and ./app/Http/Middleware/GenerateMenusForContent.php.

Actually you can setup both header and footer elements and the content of the landing view.

In the future it gonna implement à Laravel based authentication system to grant or not access to some links.

2 colors pools are implemented in /public/css/colors.css for light and dark theme.

It's an early project that ask for indulgence.

![screenshot](screenshots/landingScreen.png "landingScreen")

## Todo
### Front-end
* implement checkboxes for on-click event on links
* implement checkbox for mobile menu
* customize authentication views
### Back-end
* implement authentication system and routes
* implement lang (en, fr)
