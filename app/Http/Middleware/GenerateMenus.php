<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        \Menu::makeOnce('navBarMenuMain', function ($menu) {
            $menu->add(config('navbar-menu.navbarMenu.var1'), '/');
            $menu->add(config('navbar-menu.navbarMenu.var2'), '/');
            $menu->add(config('navbar-menu.navbarMenu.var3'), '/');
        });
        return $next($request);
    }
}
    {
        \Menu::makeOnce('footerBarMenuMain', function ($menu) {
            $menu->add(config('footer-menu.footerMenu.var1'), '/');
            $menu->add(config('footer-menu.footerMenu.var2'), '/');
            $menu->add(config('footer-menu.footerMenu.var3'), '/');
        });
}
