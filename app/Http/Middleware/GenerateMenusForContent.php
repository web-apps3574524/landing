<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class GenerateMenusForContent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        \Menu::makeOnce('menuforcontent', function ($menu) {
            $menu->add(config('content-menu.contentMenu.var1'), ['url' => '/', 'class' => 'landpage-assets-partial landpage-asset1']);
            $menu->add(config('content-menu.contentMenu.var2'), ['url' => '/', 'class' => 'landpage-assets-partial landpage-asset2']);
            $menu->add(config('content-menu.contentMenu.var3'), ['url' => '/', 'class' => 'landpage-assets-partial landpage-asset3']);
            $menu->add(config('content-menu.contentMenu.var4'), ['url' => '/', 'class' => 'landpage-assets-partial landpage-asset4']);
            $menu->add(config('content-menu.contentMenu.var5'), ['url' => '/', 'class' => 'landpage-assets-partial landpage-asset5']);
            $menu->add(config('content-menu.contentMenu.var6'), ['url' => '/', 'class' => 'landpage-assets-partial landpage-asset6']);
        });
        return $next($request);
    }
}
